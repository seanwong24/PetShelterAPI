# PetShelterAPI
The is a api server builted for PetWeatherApp which provides 3 apis:
  1. An index of Pets 
  2. Pet lookup by ID 
  3. Pet creation
  
To start it, simply navigate to the source folder where a "server.js" file exists and run "node server.js"
