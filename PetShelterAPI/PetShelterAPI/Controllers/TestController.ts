﻿/// <reference path="../Utilities/Controller.ts" />
/// <reference path="../Services/Map.ts" />

namespace Controllers {
    export class TestController extends Utilities.Controller {
        protected route: string = "/test";
        setActions() {
            this.get("", function (request, response) {
                response.end("Hello World!");
            })

            this.get("/l", async function (request, response) {
                var result = await Services.Map.getLocation(52.131627282429321, -106.68452015074649);
                response.end(result);
            })
        }
    }
}
