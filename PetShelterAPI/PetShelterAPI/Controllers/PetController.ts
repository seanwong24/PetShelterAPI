﻿/// <reference path="../Utilities/Controller.ts" />

namespace Controllers {
    export class PetController extends Utilities.Controller {
        protected route: string = "/pet";
        setActions() {
            var cors = require("cors");
            this.app.use(cors());
            var bp = require("body-parser");
            this.app.use(bp.json());
            // get all
            this.get("", async function (request, response) {
                var list = await Services.Storage.GetItemList<DomainModels.PetBase>("./DataStorage/Pets.json");
                var result: Array<DomainModels.Pet> = new Array(list.length);
                for (var i = 0; i < list.length; i++){
                    var pet = new DomainModels.Pet();
                    await pet.setProperties(list[i]);
                    result[i] = pet;
                }
                response.end(JSON.stringify(result));
            })

            // get {id}
            this.get("/:id", async function (request, response) {
                var list = await Services.Storage.GetItemList<DomainModels.PetBase>("./DataStorage/Pets.json");
                for (var i = 0; i < list.length; i++) {
                    if (list[i].id == request.params.id) {
                        var pet = new DomainModels.Pet();
                        await pet.setProperties(list[i]);
                        response.end(JSON.stringify(pet));
                        break;
                    }
                }
            })

            // add pet
            this.post("/add", async function (request, response) {
                var pet: DomainModels.PetBase = request.body;
                var info = "";
                var list = await Services.Storage.GetItemList<DomainModels.PetBase>("./DataStorage/Pets.json");
                for (var i = 0; i < list.length; i++) {
                    if (list[i].name == pet.name && list[i].breed == pet.breed) {
                        info += "name, breed";
                        break;
                    }
                }
                if (pet.latitude < -90 || pet.latitude > 90) info += (info == "" ? "latitude" : ", latitude");
                if (pet.longitude < -180 || pet.longitude > 180) info += (info == "" ? "longitude" : ", longitude");
                if (info == "") {
                    pet.id = list.length + 1;
                    Services.Storage.AddItem("./DataStorage/Pets.json", pet);
                    response.end("true");
                }
                else {
                    response.end(info);
                }
            })

        }
    }
}
