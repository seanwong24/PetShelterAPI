﻿namespace Utilities {
    export abstract class Controller {
        protected abstract route: string;
        protected app;
        init(app) {
            this.app = app;
            this.setActions();
        };
        abstract setActions();
        get(route: string, handler: (request, response) => void): void {
            this.app.get(this.route + route, handler);
        };
        put(route: string, handler: (request, response) => void): void {
            this.app.put(this.route + route, handler);
        };
        post(route: string, handler: (request, response) => void): void {
            this.app.post(this.route + route, handler);
        };
        delete(route: string, handler: (request, response) => void): void {
            this.app.delete(this.route + route, handler);
        };
    }
}
