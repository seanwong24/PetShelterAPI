﻿/// <reference path="../Services/Map.ts" />

namespace DomainModels {
    export enum PetType {
        Dog = 0,
        Cat = 1
    }
    export class PetBase {
        id: number;
        name: string;
        type: PetType;
        breed: string;
        latitude: number;
        longitude: number;
    }
    export class Pet extends PetBase {
        location: string;
        async setProperties(petBase: PetBase) {
            this.id = petBase.id;
            this.name = petBase.name;
            this.type = petBase.type;
            this.breed = petBase.breed;
            this.latitude = petBase.latitude;
            this.longitude = petBase.longitude;
            this.location = await Pet.obtainLocation(petBase);
        }
        static async obtainLocation(petBase: PetBase): Promise<string> {
            return await Services.Map.getLocation(petBase.latitude, petBase.longitude);
        }
    }
}