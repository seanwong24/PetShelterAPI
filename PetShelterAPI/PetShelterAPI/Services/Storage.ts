﻿namespace Services {
    export class Storage {
        private static fs = require("fs");

        static async GetItemList<T>(dir: string): Promise<Array<T>> {
            var data = await this.fs.readFileSync(dir, "utf8");
            var result: Array<T> = JSON.parse(data);
            return result;
        }

        static async AddItem<T>(dir: string, item: T) {
            var data = await this.GetItemList<T>(dir);
            data.push(item);
            await Storage.fs.writeFile(dir, JSON.stringify(data), null);
        }
    }
}