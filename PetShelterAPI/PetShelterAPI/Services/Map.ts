﻿namespace Services {
    export class Map {
        private static request = require("request-promise");
        static async getLocation(latitude: number, longitude: number): Promise<string> {
            var uri = "http://dev.virtualearth.net/REST/v1/Locations"
                + "/" + latitude + "," + longitude
                + "?o=json" + "&key=Ag1WJ8Q4G7kP5RB12ddoq6BDt_PwUEsVw91Izx-0cbb-afODtmcSI4DSIZPLZVT-";
            var data = await Map.request(uri);
            var d = JSON.parse(data);
            try {
                var address = d.resourceSets[0].resources[0].address;
            } catch (e) {
                return "Unknown";
            }
            return address.locality + ", " + address.adminDistrict;
        }
    }
}