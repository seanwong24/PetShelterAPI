var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var Utilities;
(function (Utilities) {
    class Controller {
        init(app) {
            this.app = app;
            this.setActions();
        }
        ;
        get(route, handler) {
            this.app.get(this.route + route, handler);
        }
        ;
        put(route, handler) {
            this.app.put(this.route + route, handler);
        }
        ;
        post(route, handler) {
            this.app.post(this.route + route, handler);
        }
        ;
        delete(route, handler) {
            this.app.delete(this.route + route, handler);
        }
        ;
    }
    Utilities.Controller = Controller;
})(Utilities || (Utilities = {}));
/// <reference path="../Utilities/Controller.ts" />
var Services;
(function (Services) {
    class ServerHost {
        constructor() {
            this.express = require("express");
            this.app = this.express();
        }
        addController(controller) {
            controller.init(this.app);
            console.log("Controller added.");
            return this;
        }
        start(port) {
            try {
                this.app.listen(port);
            }
            catch (e) {
                console.log(e);
            }
            console.log("Server started, now listening on port " + port + ".");
        }
    }
    Services.ServerHost = ServerHost;
})(Services || (Services = {}));
var Services;
(function (Services) {
    class Map {
        static getLocation(latitude, longitude) {
            return __awaiter(this, void 0, void 0, function* () {
                var uri = "http://dev.virtualearth.net/REST/v1/Locations"
                    + "/" + latitude + "," + longitude
                    + "?o=json" + "&key=Ag1WJ8Q4G7kP5RB12ddoq6BDt_PwUEsVw91Izx-0cbb-afODtmcSI4DSIZPLZVT-";
                var data = yield Map.request(uri);
                var d = JSON.parse(data);
                try {
                    var address = d.resourceSets[0].resources[0].address;
                }
                catch (e) {
                    return "Unknown";
                }
                return address.locality + ", " + address.adminDistrict;
            });
        }
    }
    Map.request = require("request-promise");
    Services.Map = Map;
})(Services || (Services = {}));
/// <reference path="../Utilities/Controller.ts" />
/// <reference path="../Services/Map.ts" />
var Controllers;
(function (Controllers) {
    class TestController extends Utilities.Controller {
        constructor() {
            super(...arguments);
            this.route = "/test";
        }
        setActions() {
            this.get("", function (request, response) {
                response.end("Hello World!");
            });
            this.get("/l", function (request, response) {
                return __awaiter(this, void 0, void 0, function* () {
                    var result = yield Services.Map.getLocation(52.131627282429321, -106.68452015074649);
                    response.end(result);
                });
            });
        }
    }
    Controllers.TestController = TestController;
})(Controllers || (Controllers = {}));
/// <reference path="../Utilities/Controller.ts" />
var Controllers;
(function (Controllers) {
    class PetController extends Utilities.Controller {
        constructor() {
            super(...arguments);
            this.route = "/pet";
        }
        setActions() {
            var cors = require("cors");
            this.app.use(cors());
            var bp = require("body-parser");
            this.app.use(bp.json());
            // get all
            this.get("", function (request, response) {
                return __awaiter(this, void 0, void 0, function* () {
                    var list = yield Services.Storage.GetItemList("./DataStorage/Pets.json");
                    var result = new Array(list.length);
                    for (var i = 0; i < list.length; i++) {
                        var pet = new DomainModels.Pet();
                        yield pet.setProperties(list[i]);
                        result[i] = pet;
                    }
                    response.end(JSON.stringify(result));
                });
            });
            // get {id}
            this.get("/:id", function (request, response) {
                return __awaiter(this, void 0, void 0, function* () {
                    var list = yield Services.Storage.GetItemList("./DataStorage/Pets.json");
                    for (var i = 0; i < list.length; i++) {
                        if (list[i].id == request.params.id) {
                            var pet = new DomainModels.Pet();
                            yield pet.setProperties(list[i]);
                            response.end(JSON.stringify(pet));
                            break;
                        }
                    }
                });
            });
            // add pet
            this.post("/add", function (request, response) {
                return __awaiter(this, void 0, void 0, function* () {
                    var pet = request.body;
                    var info = "";
                    var list = yield Services.Storage.GetItemList("./DataStorage/Pets.json");
                    for (var i = 0; i < list.length; i++) {
                        if (list[i].name == pet.name && list[i].breed == pet.breed) {
                            info += "name, breed";
                            break;
                        }
                    }
                    if (pet.latitude < -90 || pet.latitude > 90)
                        info += (info == "" ? "latitude" : ", latitude");
                    if (pet.longitude < -180 || pet.longitude > 180)
                        info += (info == "" ? "longitude" : ", longitude");
                    if (info == "") {
                        pet.id = list.length + 1;
                        Services.Storage.AddItem("./DataStorage/Pets.json", pet);
                        response.end("true");
                    }
                    else {
                        response.end(info);
                    }
                });
            });
        }
    }
    Controllers.PetController = PetController;
})(Controllers || (Controllers = {}));
/// <reference path="./Services/ServerHost.ts" />
/// <reference path="./Controllers/TestController.ts" />
/// <reference path="./Controllers/PetController.ts" />
var port = process.env.PORT || 10086;
new Services.ServerHost()
    .addController(new Controllers.TestController())
    .addController(new Controllers.PetController())
    .start(port);
var Services;
(function (Services) {
    class Storage {
        static GetItemList(dir) {
            return __awaiter(this, void 0, void 0, function* () {
                var data = yield this.fs.readFileSync(dir, "utf8");
                var result = JSON.parse(data);
                return result;
            });
        }
        static AddItem(dir, item) {
            return __awaiter(this, void 0, void 0, function* () {
                var data = yield this.GetItemList(dir);
                data.push(item);
                yield Storage.fs.writeFile(dir, JSON.stringify(data), null);
            });
        }
    }
    Storage.fs = require("fs");
    Services.Storage = Storage;
})(Services || (Services = {}));
/// <reference path="../Services/Map.ts" />
var DomainModels;
(function (DomainModels) {
    let PetType;
    (function (PetType) {
        PetType[PetType["Dog"] = 0] = "Dog";
        PetType[PetType["Cat"] = 1] = "Cat";
    })(PetType = DomainModels.PetType || (DomainModels.PetType = {}));
    class PetBase {
    }
    DomainModels.PetBase = PetBase;
    class Pet extends PetBase {
        setProperties(petBase) {
            return __awaiter(this, void 0, void 0, function* () {
                this.id = petBase.id;
                this.name = petBase.name;
                this.type = petBase.type;
                this.breed = petBase.breed;
                this.latitude = petBase.latitude;
                this.longitude = petBase.longitude;
                this.location = yield Pet.obtainLocation(petBase);
            });
        }
        static obtainLocation(petBase) {
            return __awaiter(this, void 0, void 0, function* () {
                return yield Services.Map.getLocation(petBase.latitude, petBase.longitude);
            });
        }
    }
    DomainModels.Pet = Pet;
})(DomainModels || (DomainModels = {}));
//# sourceMappingURL=server.js.map