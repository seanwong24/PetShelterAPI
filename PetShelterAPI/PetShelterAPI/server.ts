/// <reference path="./Services/ServerHost.ts" />
/// <reference path="./Controllers/TestController.ts" />
/// <reference path="./Controllers/PetController.ts" />

var port = process.env.PORT || 10086;

new Services.ServerHost()
    .addController(new Controllers.TestController())
    .addController(new Controllers.PetController())
    .start(port);